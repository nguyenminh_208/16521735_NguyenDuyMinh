#include "FillColor.h"
#include <iostream>
using namespace std;

#define INT_SIZE sizeof(Uint32) * 8 /* Integer size in bits */

#pragma region Stack

typedef Vector2D phTu;

struct Node 
{
	phTu Data;
	Node *Next;
};
typedef struct  Stack
{
	Node *Top;
};

void Init(Stack &S) //khoi tao Stack rong
{
	S.Top = 0; //Stack rong khi Top la 0
}
int Isempty(Stack S) //kiem tra Stack rong
{
	return (S.Top == 0);
}
int Len(Stack S)
{
	Node *P = S.Top;
	int i = 0;
	while (P != NULL) //trong khi chua het Stack thi van duyet
	{
		i++;
		P = P->Next;
	}
	return i;
}
Node *MakeNode(phTu x) //tao 1 Node
{
	Node *P = (Node*)malloc(sizeof(Node));
	P->Next = NULL;
	P->Data = x;
	return P;
}
void Push(Stack &S, phTu x) //them phan tu vao Stack
{
	Node *P = MakeNode(x);
	P->Next = S.Top;
	S.Top = P;
}
Vector2D Peak(Stack S) //Lay phan tu o dau Stack nhung khong xoa
{
	Node*P = S.Top;
	Vector2D v;
	v.sub(P->Data);
	return v;
}


#pragma endregion

int findHighestBitSet(Uint32 num)
{
	int order = 0, i;

	/* Iterate over each bit of integer */
	for (i = 0; i<INT_SIZE; i++)
	{
		/* If current bit is set */
		if ((num >> i) & 1)
			order = i;
	}

	return order;
}
//Get color of a pixel
SDL_Color getPixelColor(Uint32 pixel_format, Uint32 pixel)
{
    SDL_PixelFormat* fmt = SDL_AllocFormat(pixel_format);

    Uint32 temp;
    Uint8 red, green, blue, alpha;

	//Check if pixel is a 32-bit integer
	if (findHighestBitSet(pixel) == 31)
	{
		/* Get Alpha component */
		temp = pixel & fmt->Amask;  /* Isolate alpha component */
		temp = temp >> fmt->Ashift; /* Shift it down to 8-bit */
		temp = temp << fmt->Aloss;  /* Expand to a full 8-bit number */
		alpha = (Uint8)temp;
	}
	else {
		alpha = 255;
	}

	/* Get Red component */
	temp = pixel & fmt->Rmask;  /* Isolate red component */
	temp = temp >> fmt->Rshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Rloss;  /* Expand to a full 8-bit number */
	red = (Uint8)temp;

	/* Get Green component */
	temp = pixel & fmt->Gmask;  /* Isolate green component */
	temp = temp >> fmt->Gshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Gloss;  /* Expand to a full 8-bit number */
	green = (Uint8)temp;

	/* Get Blue component */
	temp = pixel & fmt->Bmask;  /* Isolate blue component */
	temp = temp >> fmt->Bshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Bloss;  /* Expand to a full 8-bit number */
	blue = (Uint8)temp;


	SDL_Color color = { red, green, blue, alpha };
	return color;



}

//Get all pixels on the window
SDL_Surface* getPixels(SDL_Window* SDLWindow, SDL_Renderer* SDLRenderer) {
	SDL_Surface* saveSurface = NULL;
	SDL_Surface* infoSurface = NULL;
	infoSurface = SDL_GetWindowSurface(SDLWindow);
	if (infoSurface == NULL) {
		std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
	}
	else {
		unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
		if (pixels == 0) {
			std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
			return NULL;
		}
		else {
			if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
				std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
				delete[] pixels;
				return NULL;
			}
			else {
				saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
			}
			delete[] pixels;
		}
	}
	return infoSurface;
}


Uint32 get_pixel32(SDL_Surface *surface, int x, int y)
{
	//Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32 *)surface->pixels;
	//Get the requested pixel
	return pixels[(y * surface->w) + x];
}

//Compare two colors
bool compareTwoColors(SDL_Color color1, SDL_Color color2)
{
    if (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
        return true;
    return false;
}

bool canFilled(SDL_Window *win, Vector2D newPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	SDL_Surface *surface = getPixels(win, ren);

	//Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32 *)surface->pixels;
	int w = surface->w;

	int index = newPoint.y * w + newPoint.x;
	Uint32 pixel = pixels[index];
	SDL_Color color = getPixelColor(pixel_format, pixel);
	cout << (int)color.r << "," << (int)color.g << "," << (int)color.b << "," << (int)color.a << endl;

	if (!compareTwoColors(color, fillColor) && !compareTwoColors(color, boundaryColor))
	{
		return true;
	}

	return false;
}



void BoundaryFill4(SDL_Window *win, Vector2D startPoint,Uint32 pixel_format,
                   SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	#pragma region đệ quy


	/*SDL_Surface *a;
	a = getPixels(win, ren);
	Uint32 pixel;
	pixel = get_pixel32(a, startPoint.x, startPoint.y);
	SDL_Color color;
	color = getPixelColor(pixel_format, pixel);
	if ((compareTwoColors(color, fillColor) == false) && (compareTwoColors(color, boundaryColor) == false))
	{
		SDL_SetRenderDrawColor(ren, color.r, color.g, color.b, color.a);
		Vector2D currentPoint;
		currentPoint = startPoint;
		SDL_RenderDrawPoint(ren, currentPoint.x, currentPoint.y);
		currentPoint.x = currentPoint.x - 1;
		BoundaryFill4(win, currentPoint, pixel_format, ren, fillColor, boundaryColor);
		currentPoint.x = currentPoint.x + 1;
		BoundaryFill4(win, currentPoint, pixel_format, ren, fillColor, boundaryColor);
		currentPoint.y = currentPoint.y - 1;
		BoundaryFill4(win, currentPoint, pixel_format, ren, fillColor, boundaryColor);
		currentPoint.y = currentPoint.y + 1;
		BoundaryFill4(win, currentPoint, pixel_format, ren, fillColor, boundaryColor);
	}*/

#pragma endregion
//
#pragma region Stack
//
	Stack DiemTo;
	Init(DiemTo);
	Len(DiemTo);
	int x = startPoint.x, y = startPoint.y;
	
	while  (Isempty(DiemTo) != NULL && x >= 0 && y >= 0)
	{

	}
//
#pragma endregion


}

//======================================================================================================================
//=============================================FILLING TRIANGLE=========================================================

int maxIn3(int a, int b, int c)
{
	int iMAX=a;
	if (iMAX < b)
		iMAX = b;
	if (iMAX < c)
		iMAX = c;
	return iMAX;
}

int minIn3(int a, int b, int c)
{
	int iMIN = a;
	if (iMIN < b)
		iMIN = b;
	if (iMIN < c)
		iMIN = c;
	return iMIN;
}

void swap(Vector2D &a, Vector2D &b)
{
	Vector2D c;
	c = a;
	a = b;
	b = c;
}

void ascendingSort(Vector2D &v1, Vector2D &v2, Vector2D &v3)
{
	Vector2D temp;
	if (v1.y > v2.y)
	{
		swap(v1, v2);
	}
	if (v1.y > v3.y)
	{
		swap(v1, v3);
	}
	if (v2.y > v3.y)
	{
		swap(v2, v3);
	}
}

void TriangleFill1(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	int xmin = minIn3(v1.x, v2.x, v3.x);
	int xmax = maxIn3(v1.x, v2.x, v3.x);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	DDA_Line(xmin, v1.y, xmax, v1.y, ren);
}

void TriangleFill2(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float const1 = (float)(v3.x - v1.x) / (v3.y - v1.y);
	float const2 = (float)(v3.x - v2.x) / (v3.y - v2.y);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float xLeft = v3.x;
	float xRight = v3.x;
	for (int y = v3.y; y <= v2.y; y--)
	{
		xLeft += const1;
		xRight += const2;
		DDA_Line(int(xLeft + 0.5), y, int(xRight + 0.5), y, ren);
	}
}

void TriangleFill3(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float const1 = (float)(v2.x - v1.x) / (v2.y - v1.y);
	float const2 = (float)(v3.x - v1.x) / (v3.y - v1.y);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float xLeft = v1.x;
	float xRight = v1.x;
	for (int y = v1.y; y <= v3.y; y++)
	{
		xLeft -= const1;
		xRight += const2;
		DDA_Line(int(xLeft + 0.5), y, int(xRight + 0.5), y, ren);
	}
}

void TriangleFill4(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float const1 = (float)(v2.x - v1.x) / (v2.y - v1.y);
	float const2 = (float)(v3.x - v1.x) / (v3.y - v1.y);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float xleft = v1.x;
	float xright = v1.x;

	for (int y = v1.y; y <= v2.y; y++)
	{
		xleft += const1;
		xright += const2;
		DDA_Line(int(xleft + 0.5), y, int(xright + 0.5), y, ren);
	}
	const1 = (float)(v3.x - v2.x) / (v3.y - v2.y);
	for ( int y = v2.y; y <= v3.y; y++)
	{
		xleft += const1;
		xright += const2;
		DDA_Line(int(xleft + 0.5), y, int(xright + 0.5), y, ren);
	}

}

void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	ascendingSort(v1, v2, v3);
	if (v1.y == v2.y&&v2.y == v3.y)
	{
		TriangleFill1(v1, v2, v3, ren, fillColor);
		return;
	}
	else if (v1.y == v2.y&&v2.y < v3.y)
	{
		TriangleFill2(v1, v2, v3, ren, fillColor);
		return;
	}
	else if (v1.y < v2.y&&v2.y == v3.y)
	{
		TriangleFill3(v1, v2, v3, ren, fillColor);
		return;
	}
	else if (v1.y< v2.y&&v2.y < v3.y)
	{
		TriangleFill4(v1, v2, v3, ren, fillColor);
		return;
	}
}

//======================================================================================================================
//===================================CIRCLE - RECTANGLE - ELLIPSE=======================================================
bool isInsideCircle(int xc, int yc, int R, int x, int y)
{
	if ((x - xc)*(x - xc) + (y - yc)*(y - yc) <= R * R)
	{
		return true;
	}
	return false;
}

void FillIntersection(int x1, int y1, int x2, int y2, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int xmin = x1 < x2 ? x1 : x2;
	int xmax = x1 > x2 ? x1 : x2;
	for (int x = xmin; x <= xmax; x++)
		if (isInsideCircle(xc, yc, R, x, y1) == true)
			SDL_RenderDrawPoint(ren, x, y1);
}

int LonNhat(int a, int b) 
{ 
	if (a>b)  
		return a;  
	return b; 
}
int NhoNhat(int a, int b) 
{ 
	if (a<b)  
		return a;  
	return b; 
}
bool CheckIntersec(int xc, int yc, int r, int x, int y)
{ 
	if ((x - xc)*(x - xc) + (y - yc)*(y - yc) <= r * r)  
		return true;  
	return false;
}

void FillIntersectionRectangleCircle(Vector2D vTopLeft, Vector2D vBottomRight, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	for (int y = vTopLeft.y; y < vBottomRight.y; y++)
	{
		int xmin = NhoNhat(vTopLeft.x, vBottomRight.x);
		int xmax = LonNhat(vTopLeft.x, vBottomRight.x);
		for (int x = xmin; x <= xmax; x++)
		{
			if (CheckIntersec(xc, yc, R, x, y) == true)
				SDL_RenderDrawPoint(ren, x, y);
		}
	}
}



void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int x = vTopLeft.x, y = vTopLeft.y;
	for (y; y <= vBottomRight.y; y++)
	{
		DDA_Line(x, y, vBottomRight.x, y, ren);
	}
}

//======================================================================================================================

void put4line(int xc, int yc, int x, int y, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	DDA_Line(xc + x, yc + y, xc - x, yc - y, ren);
	DDA_Line(xc + y, yc + x, xc - y, yc + x, ren);
	DDA_Line(xc + y, yc - x, xc - y, yc - x, ren);
	DDA_Line(xc + x, yc - y, xc - x, yc - y, ren);

}

void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = 0;
	int y = R;
	int p = 1 - R;
	put4line(xc, yc, x, y, ren, fillColor);
	while (x < y)
	{
		if (p < 0)
			p += 2 * x + 3;
		else
		{
			p += 2 * (x - y) + 5;
			y--;
		}
		x++;
		put4line(xc, yc, x, y, ren, fillColor);
	}
}

//======================================================================================================================

bool CheckIntersecEllipse(int xc, int yc, int a, int b, int x, int y)
{ 
	float sh1 = (float)(x - xc)*(x - xc) / (a*a); 
	float sh2 = (float)(y - yc)*(y - yc) / (b*b); 
	float kq = sh1 + sh2;  
	if (kq <= 1) 
		return true; 
	return false; 
}

void ToPhanGiaoEllipse(int x1, int y1, int x2, int y2, int xe, int ye, int a, int b,SDL_Renderer *ren)
{
	int xmin = NhoNhat(x1, x2);
	int xmax = LonNhat(x1, x2);
	for (int x = xmin; x <= xmax; x++)
		if (CheckIntersecEllipse(xe, ye, a, b, x, y1) == true)
			SDL_RenderDrawPoint(ren, x, y1);
}

void Put4Line1(int xc, int yc, int x, int y, int xe, int ye, int a, int b, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	ToPhanGiaoEllipse(xc + x, yc + y, xc - x, yc + y, xe, ye, a, b,ren);
	ToPhanGiaoEllipse(xc + y, yc + x, xc - y, yc + x, xe, ye, a, b,ren);
	ToPhanGiaoEllipse(xc + y, yc - x, xc - y, yc - x, xe, ye, a, b,ren); 
	ToPhanGiaoEllipse(xc + x, yc - y, xc - x, yc - y, xe, ye, a, b,ren);
}
void FillIntersectionEllipseCircle(int xcE, int ycE, int RE, int a, int b, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	
	int x = 0;
	int y = R;
	int p = 1 - R;
	Put4Line1(xc, yc, x, y, xcE, ycE, a, b, ren, fillColor);
	while (x < y)
	{
		if (p < 0)
			p += 2 * x + 3;
		else
		{
			p += 2 * (x - y) + 5;
			y--;
		}
		x++;
		Put4Line1(xc, yc, x, y, xcE, ycE, a, b, ren, fillColor);
	}
}

//======================================================================================================================

void ToPhanGiaoTron(int x1, int y1, int x2, int y2, int xc, int yc, int r, SDL_Renderer *ren)
{
	int xmin = NhoNhat(x1, x2);
	int xmax = LonNhat(x1, x2);
	for (int x = xmin; x <= xmax; x++) 
		if (CheckIntersec(xc, yc, r, x, y1) == true)
			SDL_RenderDrawPoint(ren,x, y1);
}

void Put4Line(int xc, int yc, int x, int y, int xc2, int yc2, int r2, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	ToPhanGiaoTron(xc + x, yc + y, xc - x, yc + y, xc2, yc2, r2, ren);
	ToPhanGiaoTron(xc + y, yc + x, xc - y, yc + x, xc2, yc2, r2, ren);
	ToPhanGiaoTron(xc + y, yc - x, xc - y, yc - x, xc2, yc2, r2, ren);
	ToPhanGiaoTron(xc + x, yc - y, xc - x, yc - y, xc2, yc2, r2, ren);
}

void FillIntersectionTwoCircles(int xc1, int yc1, int R1, int xc2, int yc2, int R2,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = 0;
	int y = R1;
	int P = 1 - R1;
	Put4Line(xc1, yc1, x, y, xc2, yc2, R2, ren, fillColor);
	while (x<y) 
	{ 
		if (P<0)    
			P += 2 * x + 3;
		else 
		{ 
			P += 2 * (x - y) + 5;   
			y--; 
		}  
		x++;   
		Put4Line(xc1, yc1, x, y, xc2, yc2, R2,ren,fillColor); 
	}
}
//======================================================================================================================



