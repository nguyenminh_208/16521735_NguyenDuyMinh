#pragma region buoi4_phan2
#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "Circle.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;
SDL_Window* window = NULL;
SDL_Renderer* ren = NULL;


bool CheckMouseClick(SDL_Rect *rect, int x, int y)
{
	if ((x >= rect->x) && (x <= (rect->x + rect->w)) && (y >= rect->y) && (y <= (rect->y + rect->h)))
		return true;
	else
		return false;
}

void veDiem(Vector2D p1, Vector2D p2,SDL_Renderer *ren,SDL_Rect* rect)
{
	SDL_SetRenderDrawColor(ren, 0, 255, 40, 255);
	SDL_RenderDrawRect(ren, rect);
	SDL_RenderDrawLine(ren, p1.x, p1.y, p2.x, p2.y);
}

int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);


	SDL_Rect *rect1 = new SDL_Rect();
	rect1->x = p1.x - 3;
	rect1->y = p1.y - 3;
	rect1->w = 6;
	rect1->h = 6;

	SDL_Rect *rect2 = new SDL_Rect();
	rect2->x = p2.x - 3;
	rect2->y = p2.y - 3;
	rect2->w = 6;
	rect2->h = 6;

	SDL_Rect *rect3 = new SDL_Rect();
	rect3->x = p3.x - 3;
	rect3->y = p3.y - 3;
	rect3->w = 6;
	rect3->h = 6;

	SDL_Rect *rect4 = new SDL_Rect();
	rect4->x = p4.x - 3;
	rect4->y = p4.y - 3;
	rect4->w = 6;
	rect4->h = 6;

	SDL_Color colorCurve = { 100, 20, 40, 255 }, colorRect = { 0, 255, 40, 255 };

	SDL_SetRenderDrawColor(ren, colorRect.r, colorRect.g, colorRect.b, 20);
	SDL_RenderDrawRect(ren, rect1);
	SDL_RenderDrawRect(ren, rect2);
	SDL_RenderDrawRect(ren, rect3);
	SDL_RenderDrawRect(ren, rect4);
	SDL_RenderDrawLine(ren, p1.x, p1.y, p2.x, p2.y);
	SDL_RenderDrawLine(ren, p3.x, p3.y, p2.x, p2.y);
	SDL_RenderDrawLine(ren, p3.x, p3.y, p4.x, p4.y);


	SDL_SetRenderDrawColor(ren, colorCurve.r, colorCurve.g, colorCurve.b, colorCurve.a);
	//DrawCurve2(ren, p1, p2, p3);
	DrawCurve3(ren, p1, p2, p3, p4);

	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	bool running = true;
	
	while (running)
	{
		SDL_Event event;
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				bool run = true;
				while (run)
				{
					SDL_Event event1;
					while (SDL_PollEvent(&event1))
					{
						if (event1.type == SDL_MOUSEBUTTONUP)
						{
							run = false;
						}
						if (event1.type == SDL_MOUSEMOTION)
						{
							int x, y;
							SDL_GetMouseState(&x, &y);
							Vector2D p;
							p.x = x;
							p.y = y;
							if (CheckMouseClick(rect1, x, y))
							{
								SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
								SDL_RenderClear(ren);
								p1 = p;
								SDL_SetRenderDrawColor(ren, colorCurve.r, colorCurve.g, colorCurve.b, colorCurve.a);
								DrawCurve3(ren, p1, p2, p3, p4);
								rect1->x = p1.x - 3;
								rect1->y = p1.y - 3;
								rect1->w = 6;
								rect1->h = 6;
								veDiem(p1, p2, ren, rect1);
								veDiem(p2, p3, ren, rect2);
								veDiem(p3, p4, ren, rect3);
								SDL_RenderDrawRect(ren, rect4);
								SDL_RenderPresent(ren);
							}
							if (CheckMouseClick(rect2, x, y))
							{
								SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
								SDL_RenderClear(ren);
								p2 = p;
								SDL_SetRenderDrawColor(ren, colorCurve.r, colorCurve.g, colorCurve.b, colorCurve.a);
								DrawCurve3(ren, p1, p2, p3, p4);
								rect2->x = p2.x - 3;
								rect2->y = p2.y - 3;
								rect2->w = 6;
								rect2->h = 6;
								veDiem(p1, p2, ren, rect1);
								veDiem(p2, p3, ren, rect2);
								veDiem(p3, p4, ren, rect3);
								SDL_RenderDrawRect(ren, rect4);
								SDL_RenderPresent(ren);
							}
							if (CheckMouseClick(rect3, x, y))
							{
								SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
								SDL_RenderClear(ren);
								p3 = p;
								SDL_SetRenderDrawColor(ren, colorCurve.r, colorCurve.g, colorCurve.b, colorCurve.a);
								DrawCurve3(ren, p1, p2, p3, p4);
								rect3->x = p3.x - 3;
								rect3->y = p3.y - 3;
								rect3->w = 6;
								rect3->h = 6;
								veDiem(p1, p2, ren, rect1);
								veDiem(p2, p3, ren, rect2);
								veDiem(p3, p4, ren, rect3);
								SDL_RenderDrawRect(ren, rect4);
								SDL_RenderPresent(ren);
							}
							if (CheckMouseClick(rect4, x, y))
							{
								SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
								SDL_RenderClear(ren);
								p4 = p;
								SDL_SetRenderDrawColor(ren, colorCurve.r, colorCurve.g, colorCurve.b, colorCurve.a);
								DrawCurve3(ren, p1, p2, p3, p4);
								rect4->x = p4.x - 3;
								rect4->y = p4.y - 3;
								rect4->w = 6;
								rect4->h = 6;
								veDiem(p1, p2, ren, rect1);
								veDiem(p2, p3, ren, rect2);
								veDiem(p3, p4, ren, rect3);
								SDL_RenderDrawRect(ren, rect4);
								SDL_RenderPresent(ren);
							}
						}
					}
				}
			}

			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
		}

	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}

#pragma endregion